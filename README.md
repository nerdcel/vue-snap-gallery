# vue-snap-gallery

Image slider using css snap-scroll

## Usage

run:  
```
npm i --save @nerdcel/vue-snap-gallery 
or  
yarn add @nerdcel/vue-snap-gallery
```

### In your Vue main.js:

```vue
import Vue from 'vue';  
import SnapGallery from '@nerdcel/vue-snap-gallery';

Vue.use(SnapGallery);
```

### In your template
```vue
<snap-gallery :srcset="[{src: '...'}, {...}]"
              gap="3"
              :event="customEvent"
              ref="gallery"
              :autoplay="true"
              interval="3000"
              delay="5000"
/>
```

### Component Properties
| Name   | Description                          |
|:-------|:-------------------------------------|
| srcset |`[{src: '...'}, {...}]`               |
| gap    | Defines gap between images (x * 10px)|
| event  | Custom click event on image.
|        | `(index, img) => { ...doStuff }`     |
| ref    | To call methods from outside,
|        | e.g this.$refs.gallery.goTo(2)       |
|autoplay| `true` **or** `false`                |
|interval| Autoplay interval in ms. E.g. 3000   |
|delay   | Delay for paused autoplay in ms.     |

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Jest Unit test
```
yarn test:unit
```
